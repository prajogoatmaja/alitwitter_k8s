# Ali Twitter

Twitter-like web app which can:

1. Create a tweet
2. Delete a tweet
3. List tweets

## Environment Setup
Please refer to the respective official documentation for installation guide.


- [Docker](https://docs.docker.com/docker-for-mac/install/)
- [Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)
- [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/#verifying-kubectl-configuration)
- [Helm v3.x](https://helm.sh/docs/intro/install/)

## Deploy in Kubernetes
### Setup Apps
First, deploy the Helm chart using `helm install` to create a pod in the cluster.
```bash
cd alitwitter-chart
helm install alitwitter .
```
To check the service in `minikube`, run this in the terminal
```bash
minikube service list
```
You should get a response like this
```
|-------------|--------------------------------|--------------|-----------------------------|
|  NAMESPACE  |              NAME              | TARGET PORT  |             URL             |
|-------------|--------------------------------|--------------|-----------------------------|
| default     | alitwitter-alitwitter-chart    | http/80      | http://192.168.99.100:31108 |
```

You can then run the service by running
```bash
minikube service alitwitter-alitwitter-chart --url
```
You can then access the website via accessing `<IP:port>/tweets`, for example in this case, `http://192.168.99.100:31108/tweets`
