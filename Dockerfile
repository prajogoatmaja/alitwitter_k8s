FROM ruby:2.6.5

RUN apt update && apt install -y --no-install-recommends curl
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt install -y --no-install-recommends nodejs

COPY Gemfile* /tmp/
WORKDIR /tmp
RUN gem install bundler -v '2.1.4'
RUN bundle install

WORKDIR /alitwitter
COPY . ./

RUN npm i -g yarn && yarn

EXPOSE 3000

CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
